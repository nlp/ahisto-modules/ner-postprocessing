This repository contains post-processing rules for named entities that were automatically detected in the AHISTO project.

The repository contains the following files:

- `01-structure.txt`: rules that change the structure of a paragraph by splitting and merging named entities
- `02-entity.txt`: rules that change the text of a named entity to the normalized form shown on the AHISTO web site
- `03-filtering.txt`: rules that decide which named entities will be displayed on the AHISTO web site

After updating the repository, the corresponding Git submodule in [the AHISTO project][1] should be updated as well as follows:

```
git submodule update --remote corpus/make_corpus/ner-postprocessing
git add corpus/make_corpus/ner-postprocessing
git commit -m 'Update `ner-postprocessing` submodule'
git push
```

 [1]: https://gitlab.fi.muni.cz/nlp/ahisto
